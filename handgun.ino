#include <SoftwareSerial.h>
SoftwareSerial BT(2, 3);

/*Motor*/
int ENB = 9;
int IN3 = 8;
int IN4 = 7;
int ENA = 10;
int IN1 = 12;
int IN2 = 11;
const unsigned long ULONG_MAX = 99999999;
unsigned long previous_time=ULONG_MAX;
unsigned long time_passed;

int cartridgePin = 5;
int loadedPin = 4;
int triggerPin = 3;
int cartridgeVal = 0;
int loadedVal = 0;
int triggerVal = 0;
bool isTrigger=false;
bool lockMotor=false;

//bool heartBeat = false;
bool turnOffLED;
bool ledDirty = true;

void setup() 
{
  pinMode(LED_BUILTIN, OUTPUT);
  BT.begin(9600);
  BT.println("Welcon to Arduino BT!");

  pinMode(ENB, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
}

void Motor_ENB(bool isOn)
{
  if(isOn)
  {
    analogWrite(ENB, 255);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
    analogWrite(ENA, 255);
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
  }
  else
  {
    analogWrite(ENB, LOW);
    analogWrite(ENA, LOW);
  }
}

void resetTriggerTimer()
{
  time_passed = 0;
  previous_time = millis();
}

char val;
void loop()
{
  delay(33);//control FPS
  
  cartridgeVal = analogRead(cartridgePin);
  loadedVal = analogRead(loadedPin);
  triggerVal = analogRead(triggerPin);

  /*Serial.print("cartridge : ");
  Serial.println(cartridgeVal ,DEC);
*/
  /*Serial.print("loaded : ");
  Serial.println(loadedVal ,DEC);
  */
/*
  Serial.print("trigger : ");
  Serial.println(triggerVal ,DEC);
*/
  
    if(cartridgeVal > 10)  {    BT.print('1');  }
    else  {    BT.print('0');  }
    BT.print(',');
    
    if(loadedVal < 750)  {    BT.print('1');  }
    else  {    BT.print('0');  }
    BT.print(',');
    
    if(triggerVal > 750)  {    BT.print('1'); 
                               isTrigger = true;
                               if(!lockMotor)
                               resetTriggerTimer();
                               }                          
    else                  {    BT.print('0'); 
                               lockMotor=isTrigger = false;}
    BT.println(",9");

    if(isTrigger)
    {
        lockMotor=true;
        time_passed = millis() - previous_time;        
        if(time_passed >= 100)
        {
          Motor_ENB(false);
        }
        else
        {
          Motor_ENB(true);
        }
    }
    else
    {
      Motor_ENB(false);
    }

  /*----------------------------------*/
  /*Control LED light*/
  if(cartridgeVal > 10 || loadedVal < 750 || triggerVal > 750)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    ledDirty = true;
  }
  else if(ledDirty)
  {
    ledDirty = false;
    turnOffLED = true;
  }
  
  if(turnOffLED)
  {
    turnOffLED = false;
    digitalWrite(LED_BUILTIN, LOW);
  }
  if(BT.available())
  {
    val = BT.read();    
    switch(val)
    {
      case '1':
      digitalWrite(LED_BUILTIN, HIGH);
      //heartBeat = true;
      //BT.println("LED ON");
      break;
      case '2':
      digitalWrite(LED_BUILTIN, LOW);
      //heartBeat = true;
      //BT.println("LED OFF");
      break;
    }
  }
  /*Control LED light*/  
  /*----------------------------------*/
}
