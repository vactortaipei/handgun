﻿
using MyArduino;
using UnityEngine;

public class ArduinoGunBT : ArduinoBT
{
    public const string IdentifyCode = "ArduinoGun";

    protected override void DebugLog(string text)
    {
        Debug.Log(text);
    }

    protected override void OnRecieve(string text)
    {

    }
}

public class ArduinoGun : MonoBehaviour
{
    ArduinoGunBT arduinoGunBT;// = new ArduinoGunBT();

    void Start()
    {
        arduinoGunBT = new ArduinoGunBT();
        //arduinoGunBT.InitPort(ArduinoBT.COMPORT.COM5);
        arduinoGunBT.AutoInit(ArduinoGunBT.IdentifyCode);
    }

    void Update()
    {
        arduinoGunBT.Update();

        if (Input.GetKeyDown(KeyCode.S))
            arduinoGunBT.Send("0");
        if (Input.GetKeyDown(KeyCode.D))
            arduinoGunBT.Send("1");
    }

    void OnDestroy()
    {
        arduinoGunBT.Dispose();
    }

    void OnApplicationQuit()
    {
        arduinoGunBT.Dispose();
    }
}
