﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text.RegularExpressions;
using System.Threading;

namespace MyArduino
{
    public abstract class ArduinoBT : IDisposable
    {
        public class SerialPortThread
        {
            public bool PairedSuccess = false;
            public SerialPort serialPort;
            public List<string> SendText = new List<string>();
            public List<string> RecieveText = new List<string>();
            public bool QuiteThreadDone, QuiteSendThreadDone, QuiteThread;
            public object ThreadLock = new object();
            public object SendLock = new object();
            //public object DestroyLock = new object();
            public ArduinoBT arduinoBT;
            public Thread recieveThread, sendThread;

            //public bool IsDestroy = false;
            public void Destroy()
            {
                //lock (DestroyLock)
                {
                    //IsDestroy = true;
                    QuiteThread = true;
                }
            }

            public void Send(string text)
            {
                //arduinoBT.DebugLog("[ArduinoBT][Send] : " + text);

                lock (SendLock)
                {
                    SendText.Add(text);
                }

            }

            public string[] FetchRecieve()
            {
                string[] fetch = null;
                lock (ThreadLock)
                {
                    if (RecieveText.Count > 0)
                    {
                        fetch = RecieveText.ToArray();
                        RecieveText.Clear();
                    }
                    /*
                    while (RecieveText.Count > 0)
                    {
                        arduinoBT.DebugLog("[ArduinoBT] BT recieve : " + RecieveText[0]);
                        if (RecieveText[0] == _identifyCode)
                            _identifyOK = true;
                        RecieveText.RemoveAt(0);
                    }*/
                }
                return fetch;
            }
        }

        public const string IdentifySignal = "a";
        public const string ResetSignal = "c";
        public const string PairedSuccess = "b";
        public const string PairedSuccessCode = "Paired Success";

        SerialPortThread _mainPort, _checkingPort;
        public string _identifyRecieve;
        List<string> _identifySerialPorts;
        int? _waitIdentifyMillis;
        int _recIdentifyMillis;

        protected abstract void OnRecieve(string text);
        protected abstract void DebugLog(string text);

        public enum COMPORT
        {
            COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, COM10, COM11,
        }

        public void AutoInit(string identify)
        {
            Dispose();
            _identifyRecieve = identify;
            _identifySerialPorts = new List<string>(ShowAllPorts());
        }

        public void InitPort(COMPORT port)
        {
            _mainPort = _InitPort(port, this);
        }

        public void SerialErrorReceivedEventHandler(object sender, SerialErrorReceivedEventArgs e)
        {
            DebugLog("[ArduinoBT][ErrorReceived]" + e.EventType.ToString());
        }

        SerialPortThread _InitPort(COMPORT port, ArduinoBT THIS)
        {
            try
            {
                SerialPort serialPort = new SerialPort();
                serialPort.PortName = port.ToString();
                serialPort.Parity = Parity.None;
                serialPort.BaudRate = 9600;
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                serialPort.Handshake = Handshake.None;
                //serialPort.ReadTimeout = 1; // since on windows we *cannot* have a separate read thread            
                serialPort.WriteTimeout = 1000;
                serialPort.ErrorReceived += SerialErrorReceivedEventHandler;
                serialPort.Open();

                SerialPortThread threadData = new SerialPortThread();
                threadData.serialPort = serialPort;
                threadData.QuiteThread = false;
                threadData.arduinoBT = THIS;

                threadData.recieveThread = new Thread(() => _threadLoopRecieve(threadData));
                threadData.recieveThread.Start();

                threadData.sendThread = new Thread(() => _threadLoopSend(threadData));
                threadData.sendThread.Start();

                THIS.DebugLog("[ArduinoBT][InitPort] : ");// + threadData.GetHashCode() + ", port : " + threadData.serialPort.GetHashCode());
                return threadData;
            }
            catch (Exception e)
            {
                THIS.DebugLog("[ArduinoBT][InitPort] : " + port.ToString() + " , " + e.Message);
                return null;
            }
        }

        static void _distoryPort(ref SerialPortThread threadPort)
        {
            if (threadPort == null)
                return;

            if (threadPort.PairedSuccess)
            {
                threadPort.Send(ArduinoBT.ResetSignal);//在destroy之前，傳送訊息給Arduino告知裝置可以reset參數，等下我才能再連上線
                Thread.Sleep(2000);
            }

            string portName = threadPort.serialPort.PortName;
            threadPort.arduinoBT.DebugLog("[ArduinoBT][_distoryPort] Start");
            threadPort.Destroy();

            int waitCount = 0;
            while (waitCount <= 3)
            {
                if (threadPort.QuiteThreadDone && threadPort.QuiteSendThreadDone)
                    break;
                waitCount++;
                Thread.Sleep(1000);
            }

            if (threadPort.recieveThread != null && threadPort.recieveThread.IsAlive)
            {
                threadPort.recieveThread.Abort();
                threadPort.arduinoBT.DebugLog("[ArduinoBT][recieve thread] Abort");
            }

            if (threadPort.sendThread != null && threadPort.sendThread.IsAlive)
            {
                threadPort.sendThread.Abort();
                threadPort.arduinoBT.DebugLog("[ArduinoBT][send thread] Abort");
            }
            threadPort.arduinoBT.DebugLog("[ArduinoBT][_distoryPort]" + portName + "---------Done");
            threadPort = null;
        }

        void _threadLoopSend(SerialPortThread threadData)
        {
            threadData.QuiteSendThreadDone = false;
            threadData.arduinoBT.DebugLog("[ArduinoBT][send thread] enter : ");// + threadData.GetHashCode() + ", port : " + threadData.serialPort.GetHashCode());

            while (!threadData.QuiteThread)
            {
                lock (threadData.SendLock)
                {
                    while (threadData.SendText.Count > 0)
                    {
                        try
                        {
                            threadData.serialPort.Write(threadData.SendText[0]);
                            threadData.arduinoBT.DebugLog("[ArduinoBT][send] : " + threadData.SendText[0]);
                        }
                        catch (Exception e)
                        {
                            threadData.arduinoBT.DebugLog("[ArduinoBT][send] : " + e.Message);
                        }
                        threadData.SendText.RemoveAt(0);
                    }
                }
                Thread.Sleep(0);
            }

            //lock (threadData.DestroyLock)
            {
                //if (threadData.IsDestroy)
                {
                    threadData.serialPort.Close();
                    threadData.arduinoBT.DebugLog("[ArduinoBT][send thread] serialPort.Close");
                    threadData.serialPort = null;
                }
            }

            threadData.arduinoBT.DebugLog("[ArduinoBT][send thread] exit");
            threadData.QuiteSendThreadDone = true;
        }

        void _threadLoopRecieve(SerialPortThread threadData)
        {
            threadData.QuiteThreadDone = false;

            threadData.arduinoBT.DebugLog("[ArduinoBT][recieve thread] enter");
            while (!threadData.QuiteThread)
            {
                string text = _read(threadData.serialPort);
                if (text != null)
                {
                    //只抓英文
                    //http://stackoverflow.com/questions/1181419/verifying-that-a-string-contains-only-letters-in-c-sharp
                    //http://stackoverflow.com/questions/3210393/how-do-i-remove-all-non-alphanumeric-characters-from-a-string-except-dash
                    //Only letters, numbers and underscore:
                    Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                    text = rgx.Replace(text, "");

                    if (text != null)
                    {
                        lock (threadData.ThreadLock)
                        {
                            threadData.RecieveText.Add(text);
                        }
                    }
                }
                Thread.Sleep(0); //waiting for 5 seconds
            }
            //_openThread.Abort();
            threadData.arduinoBT.DebugLog("[ArduinoBT][recieve thread] exit");
            threadData.QuiteThreadDone = true;
        }

        public void Update()
        {
            /*lock (RecieveTextLock)
            {
                while (RecieveText.Count > 0)
                {
                    DebugLog("[ArduinoBT] BT recieve : " + RecieveText[0]);

                    OnRecieve(RecieveText[0]);
                    RecieveText.RemoveAt(0);
                }
            }*/

            if (_mainPort != null)
            {
                string[] recieves = _mainPort.FetchRecieve();
                if (recieves != null)
                {
                    foreach (string r in recieves)
                    {
                        DebugLog("[ArduinoBT][Recieve] : " + r);

                        if (!_mainPort.PairedSuccess && r.ToLower().Contains(PairedSuccessCode.ToLower()))
                            _mainPort.PairedSuccess = true;

                        OnRecieve(r);
                    }
                }
            }

            if (_checkingPort != null)
            {
                string[] checkRecieves = _checkingPort.FetchRecieve();
                if (checkRecieves != null)
                {
                    foreach (string r in checkRecieves)
                    {
                        DebugLog("[ArduinoBT][自動偵測recieve] : " + r + " , [_identifyRecieve]:" + _identifyRecieve);
                        //if (r == _identifyRecieve)
                        if (r.Contains(_identifyRecieve))//因為會掉前幾個字元，所以使用contain判斷關鍵字
                        {
                            _waitIdentifyMillis = null;
                            _identifySerialPorts = null;
                            _mainPort = _checkingPort;
                            _checkingPort = null;
                            DebugLog("[ArduinoBT][自動偵測成功] : " + _mainPort.serialPort.PortName);
                            _mainPort.Send(ArduinoBT.PairedSuccess);//告訴arduino已經配對完成
                        }
                    }
                }
            }

            if (_identifySerialPorts != null && _identifySerialPorts.Count > 0 &&
                !_waitIdentifyMillis.HasValue)
            {
                COMPORT comPort = (COMPORT)Enum.Parse(typeof(COMPORT), _identifySerialPorts[0]);
                DebugLog("[ArduinoBT][自動偵測開始]" + comPort.ToString() + "------------");
                _checkingPort = _InitPort(comPort, this);
                if (_checkingPort != null)
                {
                    _recIdentifyMillis = Environment.TickCount;
                    _waitIdentifyMillis = 10000;
                    _checkingPort.Send(ArduinoBT.IdentifySignal);//跟Arduino要求傳送辨識ID
                }
                _identifySerialPorts.RemoveAt(0);
            }

            if (_waitIdentifyMillis.HasValue && _waitIdentifyMillis.Value > 0)
            {
                int elapseMillis = Environment.TickCount - _recIdentifyMillis;
                _waitIdentifyMillis -= elapseMillis;
                if (_waitIdentifyMillis < 0)
                {
                    _waitIdentifyMillis = null;
                    DebugLog("[ArduinoBT][逾時略過]" + _checkingPort.serialPort.PortName + "------------");
                    _distoryPort(ref _checkingPort);
                }
            }
        }


        static string _read(SerialPort port)
        {
            //List<byte> bytes = new List<byte>();
            // while (true)
            {
                try
                {
                    return port.ReadLine();
                    /*bytes.Add((byte)_serialPort.ReadByte());
                    if (bytes.Count == 0)
                        return null;
                    else
                        return System.Text.Encoding.UTF8.GetString(bytes.ToArray());
                        */
                }
                catch (TimeoutException e)
                {
                    //Debug.Log("[ArduinoBT] TimeoutException : " + e.Message);
                    return null;
                }
                catch (Exception e)
                {
                    //Debug.Log("[ArduinoBT] Exception : " + e.Message);
                    return null;
                }
            }
        }

        public void Dispose()
        {
            DebugLog("[ArduinoBT][Dispose] Start");
            _distoryPort(ref _mainPort);
            DebugLog("[ArduinoBT][Dispose] Done");
        }


        public static string[] ShowAllPorts()
        {
            string[] serialPorts = SerialPort.GetPortNames();

            foreach (string serialPort in serialPorts)
            {
                Console.WriteLine("[ArduinoBT][serialPort] : " + serialPort);
            }
            return serialPorts;
        }

        public void Send(string text)
        {
            _mainPort.Send(text);
            //_mainPort.serialPort.Write(text);
        }
    }

    /// <summary>
    /// 此class純粹當作smaple
    /// </summary>
    class ArduinoTest : ArduinoBT
    {
        public const string IdentifyCode = "ArduinoTest";


        object LogLock = new object();
        protected override void DebugLog(string text)
        {
            lock (LogLock)
            {
                Console.WriteLine(text);
            }
        }

        protected override void OnRecieve(string text)
        {
            Console.WriteLine("[OnRecieve] : " + text);
        }
    }
}